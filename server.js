const server = require("./app")();
const config = require('./config');

server.create(config);
server.start();
