const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const routes = require('./routes');
const mongoose = require('mongoose');

module.exports = function () {
    let server = express(),
        create,
        start;

    create = function (config) {

        server.set('env', config.env);
        server.set('port', config.port);
        server.set('hostname', config.hostname);

        server.use(bodyParser.json());
        server.use('/uploads', express.static('static/uploads'));
        server.use(cors());

        server.get('*', function (req, res, next) {
            console.log('Request was made to: ' + req.originalUrl);
            return next();
        });

        server.use('/', routes);
    };


    start = function () {
        let hostname = server.get('hostname'),
            port = server.get('port');

        // var uri = 'mongodb://localhost:27017,localhost:27018,localhost:27019/cab-booking';
        var uri = 'mongodb://localhost:27017/cab-booking';
        const client = mongoose.connect(uri, {
            useNewUrlParser: true,
            // replicaSet: 'rs0'
        })
        mongoose.set('useFindAndModify', false);
        // mongoose.set('debug', true);
        mongoose.Promise = global.Promise;

        //Get the default connection
        var db = mongoose.connection;

        //Bind connection to error event (to get notification of connection errors)
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));

        db.once('open', async function () {
            console.log('Db is Successfully Connected')
            server.listen(port, function () {
                console.log('Express server listening on - http://' + hostname + ':' + port);
            })
        });
    };

    return {
        create: create,
        start: start
    };

}