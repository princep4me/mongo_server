const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const userService = require('../../services/user');
const configConstants = require('../../config/constants');
const responseService = require('../../common/response');
const errorHandler = require('../../common/error-handler');
const validator = require('../../common/validator');
const OTP = require('../../common/otp'); 
const messages = require('../../common/messages');
const uuidv4 = require('uuid/v4')
const shortid = require('shortid');

class AuthController {

    async login(req, res) {

        let data = Object.assign({}, req.body);

        const type = this.getUserType(req.baseUrl)

        let criteria = {};

        try {

            if(data.contact_number) criteria.contact_number = data.contact_number;
            else if(data.email) {
                criteria.email = data.email;
            }
            else throw new errorHandler.BadRequestError('One or more Fields are missing');

            if(!data.password) throw new errorHandler.BadRequestError('One or more Fields are missing')
                
            let user = await userService.getUser(criteria, type);
            if(!user) throw new errorHandler.BadRequestError('Username Or Password Is Invalid')

            // User Is currently not active or not verified 
            if(user.status != 1) throw new errorHandler.UnauthorizedError('You are currently InActive or Not Verified. Please Contact the Customer Support for further help')
                
            // // Throw unauthorize if password doesn't match
            let matchBcrypt = await bcrypt.compare(data.password, user.password);
            if (! matchBcrypt) throw new errorHandler.BadRequestError('Username Or Password Is Invalid')

            // // Remove password from response
            user.password = null;

            // // Get JWT auth token and return with response
            let token = await this.getJwtAuthToken(user, type);

            let response = {
                uuid: user.id,
                user: user,
                token: token
            }

            return res.status(200).send(responseService.success(response, 'Succesfully Logged In'));
            
        } catch (err) {
            res.status(err.status || 500).send(responseService.failure(err));
        }
    }

    async register(req, res) {

        try {
            var data = Object.assign({}, req.body);
            const type = this.getUserType(req.baseUrl)

            var salt = await bcrypt.genSaltSync(10);
            var hash =  await bcrypt.hashSync(data.password, salt);

            if(!hash) throw errorHandler.InternalServerError();

            data.password = hash;
            data.unique_code = shortid.generate();

            let user = await userService.getUser({email: data.email }, type);
            if(user) {
                if(user.status == 3) await userService.deleteUser({email: data.email }, type);
                else throw new errorHandler.BadRequestError('User with this email is already registered with Us');
            } 

            user = await userService.getUser({contact_number: data.contact_number }, type);
            if(user) {
                if(user.status == 3) await userService.deleteUser({contact_number: data.contact_number }, type);
                else throw new errorHandler.BadRequestError('User with this contact Number is already registered with Us');
            }

            var userData = await userService.addUser(data, type);

            let newUser = JSON.parse(JSON.stringify(userData));
            delete newUser.password;
            delete newUser.created_at;
            delete newUser.updated_at;

            let updateDetails = {
                verification_token: uuidv4(),
            }

            let updatedUser = await userService.updateUser(updateDetails, {contact_number: data.contact_number}, type);

            // OTP.send(newUser.contact_number);
            //OTP == '123456'

            res.send(responseService.success({verification_token: updatedUser.verification_token}, messages.OTP_VIA_CONTACT_NUMBER));
        
        } catch (err) {
            res.status(err.status || 500).send(responseService.failure(err));
        }
    }

    async verifyOtp(req, res) {
        try {
            var data = Object.assign({}, req.body);
            const type = this.getUserType(req.baseUrl)

            if(!data.contact_number) throw new errorHandler.BadRequestError(messages.CONTACT_REQUIRED);
            
            if(!data.verification_token) throw new errorHandler.BadRequestError(messages.VERIFICATION_TOKEN_REQUIRED);
            
            let user = await userService.getUser({verification_token: data.verification_token}, type);
            if(!user) throw new errorHandler.BadRequestError(messages.VERIFICATION_TOKEN_INVALID)

            let verifiedUser = JSON.parse(JSON.stringify(user));
            delete verifiedUser.password;
            delete verifiedUser.created_at;
            delete verifiedUser.updated_at;
            
            // if(data.OTP === user.OTP){
            //      change the status to verfied...    
            // }

            if(data.otp === '123456') {

                let userObj = {
                    status: 1,
                    verification_token: null
                };

                await userService.updateUser(userObj, {contact_number: data.contact_number}, type);

                let token = await this.getJwtAuthToken(verifiedUser, type);

                let response = {
                    uuid: verifiedUser.id,
                    user: verifiedUser,
                    token: token
                }

                res.send(responseService.success(response))
            } else {
                throw new errorHandler.BadRequestError(messages.OTP_MISMATCH);
            }

        } catch (error) {
            res.status(error.status || 500).send(responseService.failure(error));            
        }
        
    }

    async forgetPassword(req, res) {
        
        try {
            var data = Object.assign({}, req.body);
            const type = this.getUserType(req.baseUrl)

            if(!data.contact_number) throw new errorHandler.BadRequestError(messages.CONTACT_REQUIRED);
            
            let user = await userService.getUser({contact_number: data.contact_number}, type);
            if(!user) throw new errorHandler.BadRequestError(messages.CONTACT_INVALID)

            //Send OTP 
            //Eg- 123456
            let updateDetails = {
                verification_token: uuidv4()
            }

            let updatedUser = await userService.updateUser(updateDetails, {contact_number: data.contact_number}, type);

            // OTP.send(newUser.contact_number);
            //OTP == '123456'

            res.send(responseService.success({verification_token: updatedUser.verification_token}, messages.OTP_VIA_CONTACT_NUMBER));

        } catch (error) {
            res.status(error.status || 500).send(responseService.failure(error));            
            
        }
    }

    async resetPassword(req, res) {
        
        try {
            let request = Object.assign({}, req.body);
            const type = this.getUserType(req.baseUrl)

            if(!request.password) throw new errorHandler.BadRequestError(messages.PASSWORD_REQUIRED);

            let userId = req._userInfo._user_id;

            let user = await userService.getUser({_id: userId}, type);
            if(!user) throw new errorHandler.BadRequestError(messages.PASSWORD_RESET_TOKEN_INVALID);

            var salt = await bcrypt.genSaltSync(10);
            var hash =  await bcrypt.hashSync(request.password, salt);

            if(!hash) throw errorHandler.InternalServerError();

            let updateData = {
                password: hash,
            }

            await userService.updateUser(updateData, {contact_number: user.contact_number}, type);

            res.send(responseService.success({}, messages.PASSWORD_UPDATED_SUCCESSFULLY))


        } catch (error) {
            res.status(error.status || 500).send(responseService.failure(error));                        
        }

    }

    getJwtAuthToken(user, type) {
        // Create JWT auth signature
        let jwtTokenArgs = {
            id: user._id,
            type: type,
            full_name: user.name.first + user.name.last,
            email: user.email,
            contact_number: user.contact_number
        };

        // Generate and return jwt authentication token
        return jwt.sign(jwtTokenArgs, configConstants.authSecretToken);
    }

    getUserType(url) {
        console.log('url', url);
        let type = url.split('/')[2];

        console.log('type', type)

        switch(type) {
            case 'admin': 
                return 1;
            case 'driver': 
                return 2;
            case 'passenger': 
                return 3;
            default:
                return 0;
        }
    }
}



module.exports = new AuthController();