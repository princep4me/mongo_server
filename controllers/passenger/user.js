const PassengerService = require('../../services/passenger')
const ResponseService = require('../../common/response');
const errorHandler = require('../../common/error-handler');
const Passenger = require('../../models/passenger');

class PassengerUserController {

    async getProfile(req, res) {

        try {
            let userId = req._userInfo._user_id;

            let passenger = await PassengerService.getPassenger({_id: userId})
            if(!passenger) throw new errorHandler.InternalServerError('No user Found')
            passenger.password = null;
    
            res.send(ResponseService.success(passenger));

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }

    async updateProfile(req, res) {
        
        try {
            let userId = req._userInfo._user_id;
            let request = {};
            let data = Object.assign({}, req.body);
            let profilePic = {};
            console.log('data', data);
            console.log('req.files', req.files)

            if(data.picture) data.picture = JSON.parse(data.picture);
            // delete data.picture;
            
		    if (req.files && req.files[0]) {
                profilePic = req.files.filter(ele => ele.fieldname === 'picture');
                if(profilePic) request.picture = profilePic[0].filename;
            }
            request = Object.assign(data, request)

            console.log('request', request);

            // let user =  await PassengerService.updatePassenger(request, {_id: userId});
            let user = await Passenger.findByIdAndUpdate(userId, request, {new: true})
            user.password = null;
            
            if(!user) throw new errorHandler.InternalServerError();

            let response = {
                uuid: user.id,
                user: user
            }
            // if(!user) throw new errorHandler.InternalServerError();

            res.send(ResponseService.success(response, 'Profile Updated Succesfully'))
            
        } catch (err) {
            res.status(err.status || 500).send(ResponseService.failure(err));            
        }
    }
    
}

module.exports = new PassengerUserController();