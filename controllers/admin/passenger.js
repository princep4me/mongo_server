const PassengerService = require('../../services/passenger')
const ResponseService = require('../../common/response');
const errorHandler = require('../../common/error-handler');
const HelperService = require('../../common/helper');

const ObjectId = require('mongoose').Types.ObjectId;

class AdminPassengerController {

    async getPassengers(req, res) {

        try {

            let data = Object.assign({}, req.body);

            if(!data.perPage || !data.pageNo) throw new errorHandler.BadRequestError('Pagination Variables Missing')

            let response = await PassengerService.getPassengers(data);
            let passengers = response.passengers;
            let paginationVariables = {
                total_count: response.count,
                perPage: data.perPage,
                pageNo: data.pageNo,
            }
            let count_data = {
                totalPassengerCount: response.totalPassengerCount,
                activePassengerCount: response.activePassengerCount
            }
            paginationVariables.pages = Math.ceil(paginationVariables.total_count/paginationVariables.perPage)
            res.send(ResponseService.success({passengers, paginationVariables, count_data}));

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }

    async getPassenger(req, res) {

        try{

            let id = req.params.id;
            if(!ObjectId.isValid(id)) throw new errorHandler.BadRequestError('Id is Invalid')

            let passenger = await PassengerService.getPassenger({_id: id})
            res.send(ResponseService.success(passenger))

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }

    async deletePassenger(req, res) {

        try{

            let id = req.params.id;

            console.log('id', id);

            if(!ObjectId.isValid(id)) throw new errorHandler.BadRequestError('Id is Invalid')

            let result = await PassengerService.deletePassenger({_id: id})
            console.log('result', result);
            res.send(ResponseService.success({}, 'Successfully Deleted'))

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
        
    }

    async updatePassenger(req, res) {

        try {

            let data = Object.assign({}, req.body);

            let id = req.params.id;
            if(!ObjectId.isValid(id)) throw new errorHandler.BadRequestError('Id is Invalid')
    
            let result = await PassengerService.updatePassenger(data, {_id: id});
            console.log('result', result);
    
            res.send(ResponseService.success({}, 'Updated Successfully'))

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
       

    }

}

module.exports = new AdminPassengerController();