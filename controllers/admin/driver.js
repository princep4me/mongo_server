const DriverService = require('../../services/driver')
const ResponseService = require('../../common/response');
const errorHandler = require('../../common/error-handler');

const ObjectId = require('mongoose').Types.ObjectId;

class AdminDriverController {

    async getDrivers(req, res) {

        try {

            let data = Object.assign({}, req.body);

            if(!data.perPage || !data.pageNo) throw new errorHandler.BadRequestError('Pagination Variables Missing')

            let response = await DriverService.getDrivers(data);
            let drivers = response.drivers;
            let paginationVariables = {
                total_count: response.count,
                perPage: data.perPage,
                pageNo: data.pageNo,
            }
            let count_data = {
                totalDriverCount: response.totalDriverCount,
                activeDriverCount: response.activeDriverCount
            }
            paginationVariables.pages = Math.ceil(paginationVariables.total_count/paginationVariables.perPage)
            res.send(ResponseService.success({drivers, paginationVariables, count_data}));

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }

    async getDriver(req, res) {

        try{

            let id = req.params.id;
            if(!ObjectId.isValid(id)) throw new errorHandler.BadRequestError('Id is Invalid')

            let driver = await DriverService.getDriver({_id: id})
            res.send(ResponseService.success(driver))

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }

    async deleteDriver(req, res) {

        try{

            let id = req.params.id;

            console.log('id', id);

            if(!ObjectId.isValid(id)) throw new errorHandler.BadRequestError('Id is Invalid')

            let result = await DriverService.deleteDriver({_id: id})
            console.log('result', result);
            res.send(ResponseService.success({}, 'Successfully Deleted'))

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
        
    }

    async updateDriver(req, res) {

        try {

            let data = Object.assign({}, req.body);

            let id = req.params.id;
            if(!ObjectId.isValid(id)) throw new errorHandler.BadRequestError('Id is Invalid')
    
            let result = await DriverService.updateDriver(data, {_id: id});
            console.log('result', result);
    
            res.send(ResponseService.success({}, 'Updated Successfully'))

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
       

    }

}

module.exports = new AdminDriverController();