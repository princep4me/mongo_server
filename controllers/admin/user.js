const UserService = require('../../services/user')
const ResponseService = require('../../common/response');
const errorHandler = require('../../common/error-handler');

class AdminUserController {

    async getProfile(req, res) {

        try {
            let userId = req._userInfo._user_id;

            let admin = await UserService.getAdmin({_id: userId})

            admin.password = null;
    
            res.send(ResponseService.success(admin));

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }


}

module.exports = new AdminUserController();