const DriverService = require('../../services/driver')
const ResponseService = require('../../common/response');
const errorHandler = require('../../common/error-handler');

class DriverUserController {

    async getProfile(req, res) {

        try {
            let userId = req._userInfo._user_id;

            let driver = await DriverService.getDriver({_id: userId})
            driver.password = null;

            res.send(ResponseService.success(driver));

        } catch(err) {
            res.status(err.status || 500).send(ResponseService.failure(err));
        }
    }

    async updateProfile(req, res) {
        
        try {
            let userId = req._userInfo._user_id;
            let request = {};
            let data = Object.assign({}, req.body);
            let profilePic = {};
            console.log('data', data);
            console.log('req.files', req.files)

            if(data.picture) data.picture = JSON.parse(data.picture);
            // delete data.picture;
            
		    if (req.files && req.files[0]) {
                profilePic = req.files.filter(ele => ele.fieldname === 'picture');
                if(profilePic) request.picture = profilePic[0].filename;
            }
            request = Object.assign(data, request)

            console.log('request', request);

            let user = await DriverService.updateDriver(request, {_id: userId});
            user.password = null;
            
            if(!user) throw new errorHandler.InternalServerError();

            let response = {
                uuid: user.id,
                user: user
            }
            res.send(ResponseService.success(response, 'Profile Updated Succesfully'))
            
        } catch (err) {
            res.status(err.status || 500).send(ResponseService.failure(err));            
        }
    }
}

module.exports = new DriverUserController();