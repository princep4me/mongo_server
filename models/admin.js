const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name:  {
        first: {
            type: String,
            required: true
        },
        last: {
            type: String,
            required: false
        }
    },
    email: { 
        type: String,
        required: true,
        unique: true
    },
    contact_number: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    birth_date: { 
        type: Date,
        required: false
    },
    picture: {
        type: String,
        required: false,
        default: null
    },
    gmail_id: {
        type: String,
        required: false,
        default: null
    },
    facebook_id: String,
    otp: Number, 
    access_token: String,
    verification_token: String,
    status: {
        type: Number,
        enum: [1, 2, 3, 4], // 1-> Active , 2->Inactive, 3-> Pending, 4-> Verified
        default: 2
    },
    gender: {
        type: Number,
        enum: [1, 2, 3] // 1-> Male, 2-> Female, 3-> Other
    },
}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Admin', schema);
