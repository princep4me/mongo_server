const mongoose = require('mongoose');
const messages = require('../common/messages');

var schema = new mongoose.Schema({
    name:  {
        first: {
            type: String,
            required: [true, messages.FIRST_NAME_REQUIRED]
        },
        last: {
            type: String,
            required: false
        }
    },
    email: { 
        type: String,
        required: [true, messages.EMAIL_REQUIRED],
        unique: true
    },
    contact_number: {
        type: String,
        required: [true, messages.CONTACT_REQUIRED],
        unique: true
    },
    password: {
        type: String,
        required: [true, messages.PASSWORD_REQUIRED]
    },
    birth_date: { 
        type: Date,
        required: false
    },
    picture: {
        type: String,
        required: false,
        default: null
    },
    otp: Number, 
    access_token: String,
    verification_token: String,
    status: {
        type: Number,
        enum: [1, 2, 3], // 1-> Active , 2->Inactive, 3-> Unverified
        default: 3
    },
    gender: {
        type: Number,
        enum: [1, 2, 3] // 1-> Male, 2-> Female, 3-> Other
    },
    average_rating: {
        type: mongoose.Decimal128,
    },
    wallet_balance: {
        type: mongoose.Decimal128,
        default: 0
    },
    average_rating: {
        type: mongoose.Decimal128,
        default: 0
    },
    total_rides: {
        type: Number,
        default: 0
    },
    unique_code: String

}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Passenger', schema);
