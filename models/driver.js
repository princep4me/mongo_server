const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name:  {
        first: {
            type: String,
            required: true
        },
        last: {
            type: String,
            required: false
        }
    },
    email: { 
        type: String,
        required: true,
        unique: true
    },
    contact_number: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    birth_date: { 
        type: Date,
        required: false
    },
    picture: {
        type: String,
        required: false,
        default: null
    },
    gmail_id: {
        type: String,
        required: false,
        default: null
    },
    facebook_id: String,
    otp: Number, 
    access_token: String,
    verification_token: String,
    status: {
        type: Number,
        enum: [1, 2, 3], // 1-> Active , 2->Inactive, 3->Unverified
        default: 3
    },
    gender: {
        type: Number,
        enum: [1, 2, 3] // 1-> Male, 2-> Female, 3-> Other
    },
    average_rating: {
        type: mongoose.Decimal128,
    },
    outstanding_balance: {
        type: mongoose.Decimal128,
        default: 0
    },
    car_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Car'
    },
    average_rating: {
        type: mongoose.Decimal128,
        default: 0
    },
    total_rides: {
        type: Number,
        default: 0
    },
    unique_code: String

}, {
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
});

module.exports = mongoose.model('Driver', schema);
