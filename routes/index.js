const apiRoute = require('./apis');
const express = require('express');

const router = express.Router();

router.use('/api', apiRoute);

module.exports = router;