const express = require('express');
const router = express.Router();

const authRoutes = require('./auth');
const userRoutes = require('./user');

const jwtAuth = require('../../../middlewares/jwt-auth')
const isPassenger = require('../../../middlewares/is-passenger')

router.use('/auth', authRoutes);
router.use('/', jwtAuth, isPassenger, userRoutes);

module.exports = router;
