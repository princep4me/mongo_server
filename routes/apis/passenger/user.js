const express = require('express');
const router = express.Router();

const PassengerUserController = require('../../../controllers/passenger/user')
const Upload = require('../../../middlewares/multer');

router.get('/profile', PassengerUserController.getProfile)
router.post('/update-profile', Upload.any(), PassengerUserController.updateProfile)

module.exports = router;