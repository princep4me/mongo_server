const express = require('express');
const router = express.Router();
const path = require("path").resolve;

const AuthController = require('../../../controllers/common/auth');
const jwtAuth = require(path('middlewares/jwt-auth'));
const isPassenger = require(path('middlewares/is-passenger'));

router.post('/login', AuthController.login.bind(AuthController));
router.post('/register', AuthController.register.bind(AuthController));
router.post('/verify-otp', AuthController.verifyOtp.bind(AuthController));
router.post('/forget-password', AuthController.forgetPassword.bind(AuthController));
router.post('/reset-password', jwtAuth, isPassenger, AuthController.resetPassword.bind(AuthController));

module.exports = router;