const express = require('express');
const router = express.Router();

const adminRoutes = require('./admin');
const driverRoutes = require('./driver');
const passengerRoutes = require('./passenger');
const commonRoutes = require('./common');
// const firebaseRoutes = require('./firebase-practice');

router.use('/admin', adminRoutes);
router.use('/driver', driverRoutes);
router.use('/passenger', passengerRoutes);
router.use('/', commonRoutes);
// router.use('/firebase', firebaseRoutes);

module.exports = router;