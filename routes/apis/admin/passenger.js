const express = require('express');
const router = express.Router();

const AdminPassengerController = require('../../../controllers/admin/passenger')

router.get('/:id', AdminPassengerController.getPassenger);
router.post('/', AdminPassengerController.getPassengers);
router.delete('/:id', AdminPassengerController.deletePassenger);
router.put('/:id', AdminPassengerController.updatePassenger);

module.exports = router;