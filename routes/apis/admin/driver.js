const express = require('express');
const router = express.Router();

const AdminDriverController = require('../../../controllers/admin/driver')

router.get('/:id', AdminDriverController.getDriver);
router.post('/', AdminDriverController.getDrivers);
router.delete('/:id', AdminDriverController.deleteDriver);
router.put('/:id', AdminDriverController.updateDriver);

module.exports = router;