const express = require('express');
const router = express.Router();

const AdminUserController = require('../../../controllers/admin/user')

router.get('/', AdminUserController.getProfile)

module.exports = router;
