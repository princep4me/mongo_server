const express = require('express');
const router = express.Router();

const authRoutes = require('./auth');
const userRoutes = require('./user');
const passengerRoutes = require('./passenger');
const driverRoutes = require('./driver');

const jwtAuth = require('../../../middlewares/jwt-auth')
const isAdmin = require('../../../middlewares/is-admin')

router.use('/auth', authRoutes);
router.use('/profile', jwtAuth, isAdmin, userRoutes);
router.use('/passengers', jwtAuth, passengerRoutes);
router.use('/drivers', jwtAuth, driverRoutes);

module.exports = router;
