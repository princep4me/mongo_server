const express = require('express');
const router = express.Router();

const DriverUserController = require('../../../controllers/driver/user')

const Upload = require('../../../middlewares/multer');

router.get('/profile', DriverUserController.getProfile)
router.post('/update-profile', Upload.any(), DriverUserController.updateProfile)

module.exports = router;