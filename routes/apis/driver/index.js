const express = require('express');
const router = express.Router();

const authRoutes = require('./auth');
const userRoutes = require('./user');

const jwtAuth = require('../../../middlewares/jwt-auth')
const isDriver = require('../../../middlewares/is-driver')

router.use('/auth', authRoutes);
router.use('/', jwtAuth, isDriver, userRoutes);

module.exports = router;
