const mongoose = require('mongoose');

const Driver = require('../models/driver');

class DriverService {

    async getDrivers(data) {

        let skip = data.perPage * (data.pageNo - 1);
        let limit = data.perPage;
        let drivers = {};
        let count = 0;
        let searchKeyword = data.search; 
        let totalDriverCount = 0;
        let activeDriverCount = 0;
        let condition = {};

        if(data.status == '') {
            console.log('All Customers 1')

            condition = {
                $and: 
                [ 
                    { 
                        $or:
                        [ 
                            // { contact_number: { $where: new RegExp(searchKeyword, 'i') } },
                            {contact_number: new RegExp(searchKeyword, 'i') },  
                            {"name.first": new RegExp(searchKeyword, 'i') },  
                            {"name.last": new RegExp(searchKeyword, 'i') },
                            {unique_code: new RegExp(searchKeyword, 'i') },
                            // {_id: { $regex: new RegExp("^" + searchKeyword, "i") }}  
                        ] 
                    }, 
                    { 
                        status: { $ne: 3 } 
                    }
                ]
            }

            console.log('condition', condition)
            drivers = await Driver.find(condition).select('-password').skip(skip).limit(limit);
            
            count = await Driver.countDocuments(condition);
        } else {
            console.log('condition Customers')

            condition = {
                $and: 
                [ 
                    {
                        $or:
                        [ 
                            // { contact_number: new RegExp(searchKeyword, 'i') },
                            {contact_number: new RegExp(searchKeyword, 'i') },  
                            {"name.first": new RegExp(searchKeyword, 'i') },
                            {"name.last": new RegExp(searchKeyword, 'i') },
                            {unique_code: new RegExp(searchKeyword, 'i') },
                            // {"name.first": { $regex: searchKeyword, $options: 'i'  }},  
                            // {"name.first": "/" + searchKeyword + "/i" },  
                            // {_id: { $regex: new RegExp("^" + searchKeyword, "i") }}  
                        ] 
                    }, 
                    { 
                        status: data.status 
                    }
                ]
            }
            // drivers = await Driver.find({status: data.status}).select('-password').skip(skip).limit(limit);
            drivers = await Driver.find(condition).select('-password').skip(skip).limit(limit);
            count = await Driver.countDocuments(condition);
        }

        totalDriverCount = await Driver.countDocuments();
        activeDriverCount = await Driver.countDocuments({status: 1});
        
        return {drivers, count, totalDriverCount, activeDriverCount};
    }

    async getDriver(criteria) {

        let driver = await Driver.findOne(criteria);
        // if(driver) driver.type = 'driver';
        return driver;
    }

    async addDriver(details) {
        return  new Driver(details).save();
    }

    async updateDriver(details, criteria) {
        console.log("update driver -------------------------->")
        return await Driver.findOneAndUpdate(criteria, details, {new: true})
    }

    async deleteDriver(criteria) {
        return Driver.findOneAndDelete(criteria);
    }

}

module.exports = new DriverService();