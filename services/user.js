const mongoose = require('mongoose');

const PassengerService = require('./passenger');
const DriverService = require('./driver');
const AdminService = require('./admin');

class AuthService {
    
    // async createDummyUser(req, res) {
        
    //     console.log('Serve Path Hit -----------------------------------------------------------------------------')
    //     let passenger = new Passenger ({
    //         _id: new mongoose.Types.ObjectId(),
    //         average_rating: 2.4,
    //         wallet_balance: 231.12,
    //         userId: mongoose.Types.ObjectId('5c8231fa665a1f4974996ba5'),
    //     });

    //     passenger.save().then((res) => {
    //         console.log('successful');
    //         console.log(res);
    //     })
    //     .catch((err)=> {
    //         console.log('err')
    //         console.log(err)
    //     })
    // }

    getUser(criteria, type) {
        console.log('get User', criteria);
        switch(type) {
            case 1: 
                return AdminService.getAdmin(criteria);
            case 2: 
                return DriverService.getDriver(criteria);
            case 3:
                return PassengerService.getPassenger(criteria);
            default:
                return null;
        }
    }

    async addUser(details, type) {

        switch(type) {
            case 1: 
                return AdminService.addAdmin(details);
            case 2: 
                return DriverService.addDriver(details);
            case 3:
                return PassengerService.addPassenger(details);
            default:
                return null;
        }

    }

    async updateUser(newData, criteria, type) {
        switch(type) {
            case 1: 
                return AdminService.updateAdmin(newData, criteria);
            case 2: 
                return DriverService.updateDriver(newData, criteria);
            case 3:
                return PassengerService.updatePassenger(newData, criteria);
            default:
                return null;
        }
    }

    async deleteUser(criteria, type) {
        switch(type) {
            case 2: 
                return DriverService.deleteDriver(criteria);
            case 3:
                return PassengerService.deletePassenger(criteria);
            default:
                return null;
        }
    }

    async transaction() {
        let session = null;

            session = await mongoose.startSession();
            await session.startTransaction();
            console.log('In Transaction')
            let newUser = new User ({
                _id: new mongoose.Types.ObjectId(),
                name: {
                    first: 'Admin',
                },
                email: 'admin@gmail.com',
                // contact_number: 9711669906,
                gender: 1,
                status: 1,
                password: '$2a$10$md6u1VkmLlijV0YS3WHKYe8p5lmrKmkkAr.NfhlMwOcrcglYcOgae'
            });

            try {
                let res = await newUser.save();
                
            } catch (error) {
                console.log('error ------------------------------------------>')
            }

            await session.commitTransaction();
    }
}




module.exports = new AuthService();
