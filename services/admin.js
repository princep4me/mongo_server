const mongoose = require('mongoose');

const Admin = require('../models/admin');

class AdminService {

    async getAdmin(criteria) {

        let admin = await Admin.findOne(criteria);
        // admin.type = 'admin';
        return admin;
    }

    async addAdmin(details) {
        return  new Admin(details).save();
    }

    async updateAdmin(details, criteria) {
        return  Admin.findOneAndUpdate(criteria, details, {new: true})
    }

}

module.exports = new AdminService();