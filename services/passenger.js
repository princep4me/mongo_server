const mongoose = require('mongoose');

const Passenger = require('../models/passenger');

class PassengerService {

    async getPassengers(data) {

        let skip = data.perPage * (data.pageNo - 1);
        let limit = data.perPage;
        let passengers = {};
        let count = 0;
        let searchKeyword = data.search; 
        let totalPassengerCount = 0;
        let activePassengerCount = 0;
        let condition = {};

        console.log(searchKeyword);
        console.log( "/^" + searchKeyword + "/i");

        if(data.status == '') {
            console.log('All Customers 1')

            condition = {
                $and: 
                [ 
                    { 
                        $or:
                        [ 
                            // { contact_number: { $where: new RegExp(searchKeyword, 'i') } },
                            {contact_number: new RegExp(searchKeyword, 'i') },  
                            {"name.first": new RegExp(searchKeyword, 'i') },  
                            {"name.last": new RegExp(searchKeyword, 'i') },
                            {unique_code: new RegExp(searchKeyword, 'i') },
                            // {_id: { $regex: new RegExp("^" + searchKeyword, "i") }}  
                        ] 
                    }, 
                    { 
                        status: { $ne: 3 } 
                    }
                ]
            }

            passengers = await Passenger.find(condition).select('-password').skip(skip).limit(limit);
            
            count = await Passenger.count(condition);
        } else {
            console.log('condition Customers')

            condition = {
                $and: 
                [ 
                    { 
                        $or:
                        [ 
                            // { contact_number: new RegExp(searchKeyword, 'i') },
                            {contact_number: new RegExp(searchKeyword, 'i') },  
                            {"name.first": new RegExp(searchKeyword, 'i') },
                            {"name.last": new RegExp(searchKeyword, 'i') },
                            {unique_code: new RegExp(searchKeyword, 'i') },
                            // {"name.first": { $regex: searchKeyword, $options: 'i'  }},  
                            // {"name.first": "/" + searchKeyword + "/i" },  
                            // {_id: { $regex: new RegExp("^" + searchKeyword, "i") }}  
                        ] 
                    }, 
                    { 
                        status: data.status 
                    }
                ]
            }
            // passengers = await Passenger.find({status: data.status}).select('-password').skip(skip).limit(limit);
            passengers = await Passenger.find(condition).select('-password').skip(skip).limit(limit);
            count = await Passenger.count(condition);
        }

        totalPassengerCount = await Passenger.count();
        activePassengerCount = await Passenger.count({status: 1});
        
        return {passengers, count, totalPassengerCount, activePassengerCount};
    }

    async getPassenger(criteria) {

        let passenger = await Passenger.findOne(criteria);
        // passenger.type = 'passenger';
        return passenger;
    }

    async addPassenger(details) {
        return  new Passenger(details).save();
    }

    async updatePassenger(details, criteria) {
        console.log('details-------------------------->', details)
        console.log('criteria------------------------>', criteria)
        return  await Passenger.findOneAndUpdate(criteria, details, {new: true})
    }

    async deletePassenger(criteria) {
        return Passenger.findOneAndDelete(criteria);
    }
}

module.exports = new PassengerService();