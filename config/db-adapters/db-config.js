const dbconfig = {
    development: {
      host: 'localhost',
      username: 'root',
      password: 'root',
      database: 'cab-booking',
      dialect: 'mysql'
    },
    test: {
      host: 'localhost',
      username: 'root',
      password: 'root',
      database: 'cab-booking',
      dialect: 'mysql'
    },
    production: {
      host: '',
      username: '',
      password: '',
      database: '',
      dialect: 'mysql'
    }
  }
  
  module.exports = dbconfig;