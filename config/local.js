let localConfig = {
    hostname: 'localhost',
    port: 8000,
    viewDir: './views'
};

module.exports = localConfig;