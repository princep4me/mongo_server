const path = require("path").resolve;
const messages = require(path('common/messages'))

module.exports = async (req, res, next) => {

    try {
        const type = req._userInfo._user_type
        if(type != 1) throw new Error();

        next();

    } catch (e) {
        return res.status(401).send({ messages: messages.UNAUTHORIZED_ACCESS, success: false });
    }
};