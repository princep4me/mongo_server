const path = require("path").resolve;
const messages = require(path('common/messages'))

module.exports = async (req, res, next) => {

    try {
        const type = req._userInfo._user_type
        console.log('type', type);
        if(type != 2) throw new Error();

        next();

    } catch (e) {
        console.log('Not Authenticated')
        return res.status(401).send({ messages: messages.UNAUTHORIZED_ACCESS, success: false });
    }
};