const path = require("path").resolve;
const jsonwebtoken = require('jsonwebtoken');
const config = require(path('config/constants'));
const messages = require(path('common/messages'))

module.exports = async (req, res, next) => {

    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jsonwebtoken.verify(token, config.authSecretToken);

        req._userInfo = {
            _user_id: decoded.id || undefined,
            _user_name: decoded.full_name || undefined,
            _user_email: decoded.email || undefined,
            _user_mobile: decoded.contact_number || undefined,
            _user_type: decoded.type
        };
        console.log('Authenticated...');
        next();

    } catch (e) {
        return res.status(401).send({ messages: messages.UNAUTHORIZED_ACCESS, success: false });
    }
};