const validator = {};

validator.areUndefined = function(validationObject, fields) {
    for(var i = 0; i < fields.length; i++) {
        if(validationObject[fields[i]] == undefined || validationObject[fields[i]] == '' || validationObject[fields[i]] == null) {
            return fields[i] + ' is Missing';
        }
    }

    return false;
}

module.exports = validator ;
