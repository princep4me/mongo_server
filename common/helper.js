const path = require('path').resolve;

class Helper
{
    generateOTP () {
        return Math.floor(100000 + Math.random() * 900000);
    }

    getPageSkipAndLimit(perPage, pageNo) {
        return {
            skip: perPage * pageNo,
            limit: perPage
        }
    }
}

module.exports = new Helper();