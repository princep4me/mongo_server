class AppError extends Error {
    constructor (message, status) {
    
      // Calling parent constructor of base Error class.
      super(message);
      
      // Saving class name in the property of our custom error as a shortcut.
      this.name = this.constructor.name;
  
      // Capturing stack trace, excluding constructor call from it.
      Error.captureStackTrace(this, this.constructor);

      this.status = status || 500;
      
    }
};

class BadRequestError extends AppError {
    constructor(message, errors) {
        let defaultMsg = 'Request Validation Failed. This usually happens when one or more fields are either incorrect or missing';
        super(message || defaultMsg, 400 );
        this.errors = errors || {}
    }
}

class ModelError extends AppError {
    constructor(message, errors) {
        let defaultMsg = 'Some Fields are missing';
        super(message || defaultMsg);
        this.errors = errors || {}
    }
}

class UnauthorizedError extends AppError {
    constructor(message, errors) {
        let defaultMsg = 'You are not logged in, e.g. using a valid access token.'
        super(message || defaultMsg, 401)
        this.errors = errors || {}
    }
}

class NotFoundError extends AppError {
    constructor(message, errors) {
        let defaultMsg = 'The resource you are requesting does not exist.'
        super(message || defaultMsg, 404)
        this.errors = errors || {}
    }
}

class InternalServerError extends AppError {
    constructor(message, errors) {
        let defaultMsg = 'An error occured on the server which was not the consumer\'s fault.'
        super(message || defaultMsg, 500)
        this.errors = errors || {}
    }
}

class ForeignKeyError extends AppError {
    constructor(errors) {
        let msg = 'Foreign Key Constraint Failed. PLease check your Input';
        super(msg, 500)
        this.errors = errors || {}
    }
}

module.exports = {
    BadRequestError,
    UnauthorizedError,
    NotFoundError,
    InternalServerError,
    ModelError,
    ForeignKeyError
}