class ResponseService {

    success (data = {}, message = '')
    {
        return {
            success: true,
            code: 200,
            data: data,
            message: message
        }
    }

    failure (error = {}) 
    {
        console.log(error);
        return {
            error: error.errors,
            message: error.message, 
            success: false,
            code: error.status,
        }
    }

}

module.exports = new ResponseService();