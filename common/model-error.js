const _ = require('lodash')
const modelError = {}

modelError.setErrors = function(e) {
    const validationError = {};
   
    e.errors.forEach(element => {
        if (! (element.path in validationError)) {
            validationError[element.path] = [];
        }

        validationError[element.path].push(element.message);
    });
    console.log("----------------------------------------------")
    console.log(validationError);
    console.log("----------------------------------------------")
    return validationError ;
}

module.exports = modelError;